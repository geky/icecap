# Documentation

- [Top-level README](../README.md)
- [Rendered rustdoc](https://arm-research.gitlab.io/security/icecap/html/rustdoc/)
- [Building without Docker](./building-without-docker.md)

`TODO`

In the meantime, we are eager to share and discuss any aspect of IceCap's design
and implementation with you directly. Please feel free to reach out to project
lead [Nick Spinale &lt;nick.spinale@arm.com&gt;](mailto:nick.spinale@arm.com).
