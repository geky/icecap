from icedl.common import ElfComponent

class Idle(ElfComponent):

    def arg(self):
        return self.empty_arg()
