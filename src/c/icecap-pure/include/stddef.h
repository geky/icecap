#pragma once

typedef unsigned long uintptr_t;
typedef unsigned long size_t;
typedef signed long ssize_t;

#define NULL 0
