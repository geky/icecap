{ mkExclude }:

mkExclude {
  nix.name = "absurdity";
  nix.buildScriptHack = true;
}
