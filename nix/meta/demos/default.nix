{
  minimal-root = ./minimal-root;
  minimal = ./minimal;
  realm-mirage = ./realm-mirage;
  realm-vm = ./realm-vm;
}
